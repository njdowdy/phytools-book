## Chapter 8: Reconstructing ancestral states 

### Prepare for analyses
```{r chunk0}
library(phytools)
library(geiger)
set.seed(1234)
```

The endeavor of determining what ancestral species were like using a phylogenetic tree is called ancestral state reconstruction (ASR) (reviewed in Felsenstein 2004; Harmon 2019).

In this chapter, we will:
1. Learn to estimate ancestral states for continuous characters using maximum likelihood (and 95% confidence intervals).
2. Examine statistical properties of maximum likelihood estimation of these using numerical simulation
3. Learn about reconstructing discrete characters and the distinction between joint and marginal estimation
4. Explore discrete character mapping via stochastic character mapping
5. Examine why maximum parsimony should not be used for ASR

### Ancestral states for continuous characters

To estimate ancestral states for the internal nodes of our phylogeny, we’ll need to make some assumptions about the evolutionary process.

The most common model to use in ASR is Brownian motion.

#### Estimating ancestral states under Brownian motion

To estimate ancestral states under Brownian motion, our tactic will be to identify the set of states that have the highest probability under our assumed model (Felsenstein 2004).

These states will be, by definition, our maximum likelihood estimates (MLEs; see Harmon 2019).

We will start out by estimating ancestral body size of elopomorph eels.

```{r chunk1}
## read tree from file
eel.tree <- read.tree("elopomorph.tre")
print(eel.tree, printlen = 2)
## read data
eel.data <- read.csv("elopomorph.csv",
    row.names = 1,
    stringsAsFactors = TRUE
)
head(eel.data)
```

```{r chunk2}
## extract total body length and log-transform
lnTL <- setNames(log(eel.data$Max_TL_cm), rownames(eel.data))
head(lnTL)
```
```{r chunk3}
## estimate ancestral states using fastAnc
fit.lnTL <- fastAnc(eel.tree, lnTL, vars = TRUE, CI = TRUE)
print(fit.lnTL, printlen = 10)
```
```{r chunk4}
## plot eel phylogeny using plotTree
plotTree(eel.tree, ftype = "i", fsize = 0.5, lwd = 1)
## add node labels for reference
labelnodes(1:eel.tree$Nnode + Ntip(eel.tree),
    1:eel.tree$Nnode + Ntip(eel.tree),
    interactive = FALSE, cex = 0.5
)
```

#### Plotting reconstructed ancestral states on the tree


```{r chunk5}
## compute "contMap" object
eel.contMap <- contMap(eel.tree, lnTL,
    plot = FALSE, lims = c(2.7, 5.8)
)
## change the color gradient to a custom gradient
eel.contMap <- setMap(
    eel.contMap,
    c("white", "orange", "black")
)
## plot "contMap" object
plot(eel.contMap,
    sig = 2, fsize = c(0.4, 0.7),
    lwd = c(2, 3), leg.txt = "log(total length cm)"
)
```

```{r chunk6}
## identify the tips descended from node 102
tips <- extract.clade(eel.tree, 102)$tip.label
tips
## prune "contMap" object to retain only these tips
pruned.contMap <- keep.tip.contMap(eel.contMap, tips)
## plot object
plot(pruned.contMap,
    xlim = c(-2, 90), lwd = c(3, 4),
    fsize = c(0.7, 0.8)
)
## add error bars
errorbar.contMap(pruned.contMap, lwd = 8)
```
#### Properties of ancestral state estimation for continuous traits

```{r chunk7}
## simulate a tree & some data
tree <- pbtree(n = 26, scale = 1, tip.label = LETTERS)
## simulate with ancestral states
x <- fastBM(tree, internal = TRUE)
```

```{r chunk8}
## ancestral states
a <- x[1:tree$Nnode + Ntip(tree)]
## tip data
x <- x[tree$tip.label]
```

```{r chunk9}
## estimate ancestral states for simulated data
fit <- fastAnc(tree, x, CI = TRUE)
print(fit, printlen = 6)
```

```{r chunk10}
par(mar = c(5.1, 4.1, 2.1, 2.1))
plot(a, fit$ace,
    xlab = "true states", ylab = "estimated states",
    bty = "n", bg = "gray", cex = 1.5, pch = 21, las = 1, cex.axis = 0.8
)
```

```{r chunk11}
## set margins
par(mar = c(5.1, 4.1, 2.1, 2.1))
## plot true vs. estimated states
plot(a, fit$ace,
    xlab = "true states",
    ylab = "estimated states", bty = "n",
    ylim = range(fit$CI95), col = "transparent",
    las = 1, cex.axis = 0.8
)
## add 1:1 line
lines(range(c(x, a)), range(c(x, a)),
    col = "red", lwd = 2
) ## 1:1 line
## use a for loop to add vertical lines showing
## each confidence interval
for (i in 1:tree$Nnode) {
    lines(rep(a[i], 2), fit$CI95[i, ],
        lwd = 5,
        col = make.transparent("blue", 0.25),
        lend = 0
    )
}
## create a legend
legend(
    x = "topleft", legend = c(
        "95% CI for ancestral state",
        "1:1 line"
    ), col = c(
        make.transparent("blue", 0.25),
        "red"
    ), lty = c("solid", "solid"), lwd = c(5, 2), cex = 0.7,
    bty = "n"
)
```

```{r chunk12}
withinCI <- ((a >= fit$CI95[, 1]) & (a <= fit$CI95[, 2]))
table(withinCI)
mean(withinCI)
```

```{r chunk13}
## custom function that conducts a simulation, estimates
## ancestral states, & returns the fraction on 95% CI
foo <- function() {
    tree <- pbtree(n = 100)
    x <- fastBM(tree, internal = TRUE)
    fit <- fastAnc(tree, x[1:length(tree$tip.label)],
        CI = TRUE
    )
    withinCI <- ((x[1:tree$Nnode + length(tree$tip.label)] >=
        fit$CI95[, 1]) &
        (x[1:tree$Nnode + length(tree$tip.label)] <=
            fit$CI95[, 2]))
    mean(withinCI)
}
## conduct 100 simulations
pp <- replicate(100, foo())
mean(pp)
```

####  What happens when the model is wrong?

```{r chunk14}
## simulate a vector of data under a trend model
y <- fastBM(tree, mu = 2, internal = TRUE)
```

```{r chunk15}
## set margins
par(mar = c(5.1, 4.1, 2.1, 2.1))
## plot traitgram with known states
phenogram(tree, y,
    fsize = 0.6,
    color = make.transparent("blue", 0.5),
    spread.cost = c(1, 0), cex.axis = 0.8,
    las = 1
)
```

```{r chunk16}
## estimate ancestral states
fit.trend <- fastAnc(tree, y[tree$tip.label], CI = TRUE)
## set margins
par(mar = c(5.1, 4.1, 2.1, 2.1))
## create plot showing true states vs. estimated
## states
plot(a <- y[1:tree$Nnode + Ntip(tree)], fit.trend$ace,
    xlab = "true states",
    ylab = "estimated states", bty = "n",
    ylim = range(fit.trend$CI95), col = "transparent",
    cex.axis = 0.8, las = 1
)
## add 1:1 line & lines showing CI of each estimate
lines(range(y), range(y),
    lty = "dashed",
    col = "red"
) ## 1:1 line
for (i in 1:tree$Nnode) {
    lines(rep(a[i], 2), fit.trend$CI95[i, ],
        lwd = 5,
        col = make.transparent("blue", 0.25),
        lend = 0
    )
}
points(a, fit.trend$ace, bg = "grey", cex = 1.5, pch = 21)
## plot legend
legend(
    x = "bottomright",
    legend = c(
        "estimated ancestral state",
        "95% CI for ancestral state",
        "1:1 line"
    ), cex = 0.7, col = c(
        "black",
        make.transparent("blue", 0.25),
        "red"
    ), pch = c(21, NA, NA), pt.bg = c("grey", NA, NA),
    pt.cex = c(1.5, NA, NA), bty = "n",
    lty = c(NA, "solid", "dashed"), lwd = c(NA, 5, 1)
)
```

### Discrete characters

```{r chunk17}
## extract feeding mode as a vector
feed.mode <- setNames(eel.data[, 1], rownames(eel.data))
## set colors for plotting
cols <- setNames(c("red", "lightblue"), levels(feed.mode))
## plot the tree & data
plotTree.datamatrix(eel.tree, as.data.frame(feed.mode),
    colors = list(cols), header = FALSE, fsize = 0.45
)
## add legend
legend("topright",
    legend = levels(feed.mode), pch = 22,
    pt.cex = 1.5, pt.bg = cols, bty = "n", cex = 0.8
)
```

#### Choosing a character model


```{r chunk18}
## fit ER model
fitER <- fitMk(eel.tree, feed.mode, model = "ER")
## fit ARD model
fitARD <- fitMk(eel.tree, feed.mode, model = "ARD")
## fit bite->suction model
fit01 <- fitMk(eel.tree, feed.mode,
    model = matrix(c(0, 1, 0, 0), 2, 2, byrow = TRUE)
)
## fit suction->bite model
fit10 <- fitMk(eel.tree, feed.mode,
    model = matrix(c(0, 0, 1, 0), 2, 2, byrow = TRUE)
)
## extract AIC values for each model
aic <- c(AIC(fitER), AIC(fitARD), AIC(fit01), AIC(fit10))
## print summary table
data.frame(
    model = c(
        "ER", "ARD", "bite->suction",
        "suction->bite"
    ),
    logL = c(
        logLik(fitER), logLik(fitARD),
        logLik(fit01), logLik(fit10)
    ),
    AIC = aic, delta.AIC = aic - min(aic)
)
```

#### Joint vs. marginal ancestral state reconstruction

### Joint ancestral state reconstruction

```{r chunk19}
library(corHMM)
## create new data frame for corHMM
eel.data <- data.frame(
    Genus_sp = names(feed.mode),
    feed.mode = as.numeric(feed.mode) - 1
)
head(eel.data, n = 10)
```

```{r chunk20}
## estimate joint ancestral states using corHMM
fit.joint <- corHMM(eel.tree, eel.data,
    node.states = "joint",
    rate.cat = 1, rate.mat = matrix(c(NA, 1, 1, NA), 2, 2)
)
fit.joint
```

```{r chunk21}
## plot the tree & data
plotTree.datamatrix(eel.tree, as.data.frame(feed.mode), colors = list(cols), header = FALSE, fsize = 0.45)
## add legend
legend("topright",
    legend = levels(feed.mode), pch = 22,
    pt.cex = 1.5, pt.bg = cols, bty = "n", cex = 0.8
)
## add node labels showing ancestral states
nodelabels(pie = to.matrix(
    levels(feed.mode)[fit.joint$phy$node.label],
    levels(feed.mode)
), piecol = cols, cex = 0.4)
```

### Marginal ancestral state reconstruction


```{r chunk22}
## estimate marginal ancestral states under a ER model
fit.marginal <- corHMM(eel.tree, eel.data,
    node.states = "marginal",
    rate.cat = 1, rate.mat = matrix(c(NA, 1, 1, NA), 2, 2)
)
fit.marginal
head(fit.marginal$states)
```

```{r chunk23}
## plot the tree & data
plotTree.datamatrix(eel.tree, as.data.frame(feed.mode),
    colors = list(cols), header = FALSE, fsize = 0.45
)
## add legend
legend("topright",
    legend = levels(feed.mode), pch = 22,
    pt.cex = 1.5, pt.bg = cols, bty = "n", cex = 0.8
)
## add node labels showing marginal ancestral states
nodelabels(
    pie = fit.marginal$states, piecol = cols,
    cex = 0.5
)
```

### Stochastic character mapping

```{r chunk24}
## generate one stochastic character history
mtree <- make.simmap(eel.tree, feed.mode, model = "ER")
## plot single stochastic map
plot(mtree, cols,
    fsize = 0.4, ftype = "i", lwd = 2, offset = 0.4,
    ylim = c(-1, Ntip(eel.tree))
)
## add legend
legend("bottomleft",
    legend = levels(feed.mode), pch = 22,
    pt.cex = 1.5, pt.bg = cols, bty = "n", cex = 0.8
)
```

```{r chunk25}
## generate 1,000 stochastic character maps in which
## the transition rate is sampled from its posterior
## distribution
mtrees <- make.simmap(eel.tree, feed.mode,
    model = "ER",
    nsim = 1000, Q = "mcmc", vQ = 0.01,
    prior = list(use.empirical = TRUE), samplefreq = 10
)
mtrees
```

```{r chunk26}
## set plot margins
par(mar = c(5.1, 4.1, 2.1, 2.1))
## create a plot of the posterior density from stochastic
## mapping
plot(d <- density(sapply(mtrees, function(x) x$Q[1, 2]),
    bw = 0.005
),
bty = "n", main = "", xlab = "q", xlim = c(0, 0.5),
ylab = "Posterior density from MCMC", las = 1,
cex.axis = 0.8
)
polygon(d, col = make.transparent("blue", 0.25))
## add line indicating ML solution for the same parameter
abline(v = fit.marginal$solution[1, 2])
text(
    x = fit.marginal$solution[1, 2], y = max(d$y), "MLE(q)",
    pos = 4
)
```

```{r chunk27}
## create a 10 x 10 grid of plot cells
par(mfrow = c(10, 10))
## graph 100 stochastic map trees, sampled evenly from
## our set of 1,000
null <- sapply(mtrees[seq(10, 1000, by = 10)],
    plot,
    colors = cols, lwd = 1, ftype = "off"
)
```

```{r chunk28}
## compute posterior probabilities at nodes
pd <- summary(mtrees)
pd
```

```{r chunk29}
## create a plot showing PP at all nodes of the tree
plot(pd,
    colors = cols, fsize = 0.4, ftype = "i", lwd = 2,
    offset = 0.4, ylim = c(-1, Ntip(eel.tree)),
    cex = c(0.5, 0.3)
)
## add a legend
legend("bottomleft",
    legend = levels(feed.mode), pch = 22,
    pt.cex = 1.5, pt.bg = cols, bty = "n", cex = 0.8
)
```

```{r chunk30}
## set margins
par(mar = c(5.1, 4.1, 2.1, 2.1))
## graph marginal ancestral states and posterior
## probabilities from stochastic mapping
plot(fit.marginal$states, pd$ace[1:eel.tree$Nnode],
    pch = 21,
    cex = 1.2, bg = "grey", xlab = "Marginal scaled likelihoods",
    ylab = "Posterior probabilities",
    bty = "n", las = 1, cex.axis = 0.8
)
lines(c(0, 1), c(0, 1), col = "blue", lwd = 2)
```

```{r chunk31}
## create a "densityMap" object
eel.densityMap <- densityMap(mtrees,
    states = levels(feed.mode)[2:1], plot = FALSE
)
```


```{r chunk32}
## update color gradient
eel.densityMap <- setMap(eel.densityMap, cols[2:1])
## plot it, adjusting the plotting parameters
plot(eel.densityMap, fsize = c(0.3, 0.7), lwd = c(3, 4))
```

### What about parsimony?


```{r chunk33}
## set Q to its MLE
fixed.Q <- mtree$Q
mtree.mle <- make.simmap(eel.tree, feed.mode,
    Q = fixed.Q
)
```

```{r chunk34}
## set Q to a value 1/1000 smaller
mtree.slow <- make.simmap(eel.tree, feed.mode,
    Q = 0.001 * fixed.Q
)
```

```{r chunk35}
## plot the results
par(mfrow = c(1, 2))
plot(mtree.mle, cols, ftype = "off")
plot(mtree.slow, cols, ftype = "off")
```

## Practice Problems

### 8.1

Investigate your reconstructed ancestral character states for the continuous character in the eel data set, log-transformed total length ("lnTL"). In particular, determine how many ancestral eels we can confidently reconstruct as longer than 80 cm. That is, how many nodes have a confidence interval for the ancestral state that is larger than, and excludes, 80 cm?

### 8.2

Create a new discrete character, bigOrSmall, for the anoles using the data sets from chapter 1 (anole.data.csv and Anolis.tre). Assign a species to the state "big" if lnSVL > 4 and "small" otherwise. Estimate ancestral states for your new character under an appropriate model. Finally, compare the ancestral states for your new character with those considering size as a continuous character. Are the anoles reconstructed as "big" also reconstructed to have a large lnSVL?

### 8.3

Explore the accuracy of ancestral state reconstruction when data are simulated under an OU model but reconstructed using Brownian motion. What happens when we get the model wrong in this way? Do enough simulations—and follow-up analyses—so that you can see the general pattern!