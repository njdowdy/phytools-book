## Chapter 1: A Brief Introduction to Phylogenetics in R

### Prepare for analyses

```{r chunk1}
set.seed(1234)
library(phytools)
```

### Reading and Writing Phylogenetic Trees

```{r chunk2}
anolis.tree <- read.tree(file = "Anolis.tre")
anolis.tree
```

### Plot Rectangular Trees

```{r }
plotTree(anolis.tree, ftype = "i", fsize = 0.4, lwd = 1)
```

### Pruning Taxa

```{r }
pr.species <- c(
    "cooki", "poncensis", "gundlachi", "pulchellus",
    "stratulus", "krugi", "evermanni", "occultus", "cuvieri", "cristatellus"
)
nodes <- sapply(pr.species, grep, x = anolis.tree$tip.label)
```

### Plot Circular Trees

```{r }
plotTree(anolis.tree, type = "fan", fsize = 0.6, lwd = 1, ftype = "i")
```

### Highlighting Tips

```{r }
plotTree(anolis.tree, type = "fan", fsize = 0.6, lwd = 1, ftype = "i")
add.arrow(anolis.tree, tip = nodes, arrl = 0.15, col = "red", offset = 2)
```

### Extract a Clade by MRCA

#### Select MRCA of Taxa

Select the clade of Puerto Rican anoles (excluding the distantly related *cuvieri* and *occultus*). 

These taxa are excluded because they shift the MRCA too far back in time and cause inclusion of too many non-Puerto Rican species.

```{r }
node <- getMRCA(
    anolis.tree,
    pr.species[-which(pr.species %in% c("cuvieri", "occultus"))]
)
```

#### Highlighting Subclade by MRCA

```{r }
plot(paintSubTree(anolis.tree, node, " b ", " a "),
    type = "fan", fsize = 0.6, lwd = 2,
    colors = setNames(c(" gray ", " blue "), c(" a ", " b ")), ftype = "i"
)
arc.cladelabels(anolis.tree, " clade to extract ",
    node, 1.30, 1.35,
    mark.node = FALSE, cex = 0.6
)
```

#### Extract & Plot Subclade by MRCA

Plot the clade of Puerto Rican anoles: <br/>
1. Excluding the distantly related *cuvieri* and *occultus* <br/>
2. Adding the excluded taxa *cuvieri* and *occultus* back in to be a complete Puerto Rican anole phylogeny <br/>

```{r }
pr.clade <- extract.clade(anolis.tree, node)
pr.tree <- keep.tip(anolis.tree, pr.species)
par(mfrow = c(1, 2))
plotTree(pr.clade, ftype = "i", mar = c(1.1, 1.1, 3.1, 1.1), cex = 1.1)
mtext("(a)", line = 0, adj = 0)
plotTree(pr.tree, ftype = "i", mar = c(1.1, 1.1, 3.1, 1.1), cex = 1.1)
mtext("(b)", line = 0, adj = 0)
```

### Loading trait data from csv.

```{r }
anole.data <- read.csv(file = "anole.data.csv", row.names = 1, header = TRUE)
ecomorph <- read.csv(
    file = "ecomorph.csv",
    row.names = 1, header = TRUE, stringsAsFactors = TRUE
)
```

### Pruning a tree to match trait data, and vice versa
```{r }
library(geiger)
chk <- name.check(anolis.tree, ecomorph)
ecomorph.tree <- drop.tip(anolis.tree, chk$tree_not_data)
ecomorph.data <- anole.data[ecomorph.tree$tip.label, ]
name.check(ecomorph.tree, ecomorph.data)
```

### Phylogenetic principal components analysis
```{r }
ecomorph.pca <- phyl.pca(ecomorph.tree, ecomorph.data)
ecomorph.pca ## print statistical results table
par(
    mar = c(4.1, 4.1, 2.1, 1.1),
    las = 1
) ## set margins plot(ecomorph.pca,main="")
plot(ecomorph.pca, main = "")
```

### Plot Phylomorphospace
```{r }
par(cex.axis = 0.8, mar = c(5.1, 5.1, 1.1, 1.1))
phylomorphospace(ecomorph.tree,
    scores(ecomorph.pca)[, 1:2],
    ftype = "off", node.size = c(0, 1),
    bty = "n", las = 1, xlab = "PC1 (overall size)",
    ylab = expression(paste(
        "PC2 (" %up% "lamellae number, " %down% "tail length)"
    ))
)
eco <- setNames(ecomorph[, 1], rownames(ecomorph))
ECO <- to.matrix(eco, levels(eco))
tiplabels(pie = ECO[ecomorph.tree$tip.label, ], cex = 0.5)
legend(
    x = "bottomright", legend = levels(eco),
    cex = 0.8, pch = 21, pt.bg = rainbow(n = length(levels(eco))), pt.cex = 1.5
)
```

## PRACTICE PROBLEMS

### Practice Problem 1.1

Download the following two files for *Phelsuma* geckos from the book website: `phel.csv` and `phel.phy` (Harmon et al. 2010). 

`phel.csv` is a CSV file containing trait values for ten different morphological traits. `phel.phy` is a phylogeny of thirty-three species.

Read both data and tree in from file and use `name.check` to identify any differences between the two data sets.

If you find differences, prune the phylogeny and subsample the trait data to include only the species present in both the data file and the tree.

Plot the tree.

``` {r }
# read in tree
# read in data
# identify overlap between datasets
# prune phylogeny
# subsample trait data
# plot tree(s)
```

### Practice Problem 1.2

Use `phyl.pca` to run a phylogenetic principal component analysis (PCA) of the morphological data set and tree from practice problem 1.1.

When data for different variables in a PCA have different orders of magnitude, it often makes sense to transform by the natural logarithm and conduct our analysis on the log-transformed values instead of on the original traits.

Inspect your data to see if this applies and then decide whether or not to log-transform before undertaking your phylogenetic PCA.

After you’ve obtained a result for the PCA, create a screeplot to visualize the distribution of variation between the different principal component axes. 

``` {r }
# run pPCA
# log-transform data & re-run pPCA
# plot screeplot
```

### Practice Problem 1.3

Use `phylomorphospace` to create a single projection of the phylogeny into morphospace for the first two PC axes from practice problem 1.2.

Can you think of a way to project the tree into a space defined by more than two principal component dimensions?

Hint: look up the help pages for `phylomorphospace3d` and `phyloScattergram` for ideas, or consider simply subdividing your plotting device using `par(mfrow)`. 

``` {r }
# set up multi-plot parameters
# plot phylomorphospace of PC1 & PC2
# plot phylomorphospace of multiple PC's
```