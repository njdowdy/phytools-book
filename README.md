# phytools-book

Code and data for "Phylogenetic Comparative Methods in R" (Revell & Harmon, 2022).

## Book Information

[Publisher Info](https://press.princeton.edu/books/paperback/9780691219035/phylogenetic-comparative-methods-in-r)

<img src="https://pup-assets.imgix.net/onix/images/9780691219035.jpg" height="300">

## Chapters

### 1. A brief introduction to phylogenetics in R 
### 2. Phylogenetically independent contrasts
### 3. Phylogenetic generalized least squares
### 4. Modeling continuous character evolution on a phylogeny
### 5. Multi-rate, multi-regime, and multivariate models for continuous traits
### 6. Modeling discrete character evolution on a phylogeny
### 7. Other models of discrete character evolution
### 8. Reconstructing ancestral states
### 9. Analysis of diversification with phylogenies
### 10. Time- and density-dependent diversification
### 11. Character-dependent diversification
### 12. Biogeography and phylogenetic community ecology
### 13. Plotting phylogenies and comparative data