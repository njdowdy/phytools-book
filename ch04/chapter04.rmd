## Chapter 4: Modeling continuous character evolution on a phylogeny

### Prepare for analyses

```{r chunk0}
library(phytools)
library(geiger)
set.seed(1234)
```

### The Brownian motion model

"Brownian Motion" is one simple model of character evolution which is a stochastic continuous-time random walk that changes from one time period to the next by randomly drawing from a normal distribution with a mean of zero
and a variance σ^2^.

The expected variance under Brownian motion increases linearly through time with an instantaneous rate equivalent to the variance of the normal distribution from which the evolutionary changes are drawn (*i.e.*, σ^2^).

```{r chunk1}
# set values for time steps and sigma squared parameter
t <- 0:100
sig2 <- 0.01
## simulate a set of random changes
x <- rnorm(n = length(t) - 1, sd = sqrt(sig2))
## compute their cumulative sum
x <- c(0, cumsum(x))
# create a plot with nice margins
par(mar = c(5.1, 4.1, 2.1, 2.1))
plot(t, x,
    type = "l", ylim = c(-2, 2), bty = "n",
    xlab = "time", ylab = "trait value", las = 1,
    cex.axis = 0.8
)
```

```{r chunk2}
# set number of simulations
nsim <- 100
# create matrix of random normal deviates
x <- matrix(
    rnorm(n = nsim * (length(t) - 1), sd = sqrt(sig2)),
    nsim, length(t) - 1
)
# calculate the cumulative sum of these deviates
# this is now a simulation of Brownian motion
x <- cbind(rep(0, nsim), t(apply(x, 1, cumsum)))
# plot the first one
par(mar = c(5.1, 4.1, 2.1, 2.1))
plot(t, x[1, ],
    ylim = c(-2, 2), type = "l", bty = "n",
    xlab = "time", ylab = "trait value", las = 1,
    cex.axis = 0.8
)
# plot the rest
invisible(apply(x[2:nsim, ], 1, function(x, t) lines(t, x),
    t = t
))
```

Overall, there is no collective tendency to evolve up or down. It is a "random walk".  In general, the variation among simulations starts off small and increases as time elapses from left to right on our graph.

The Brownian process depends on σ^2^, the instantaneous rate of the process. We can see how this parameter affects the model outcome.

### Properties of Brownian motion

1. Stochastic, directionless change over time
2. Variation accumulates over time
3. More closely related species have more similar phenotypes

We can verify point #3 using simulations.

```{r chunk3}
# create matrix of random normal deviates
# but with a smaller sd
x <- matrix(
    rnorm(n = nsim * (length(t) - 1), sd = sqrt(sig2 / 10)),
    nsim, length(t) - 1
)
# calculate the cumulative sum of these changes
# this is now a simulation of Brownian motion
x <- cbind(rep(0, nsim), t(apply(x, 1, cumsum)))
# plot as above
par(mar = c(5.1, 4.1, 2.1, 2.1))
plot(t,
    x[1, ],
    ylim = c(-2, 2),
    type = "l",
    bty = "n",
    xlab = "time",
    ylab = "trait value",
    las = 1,
    cex.axis = 0.8
)
invisible(apply(x[2:nsim, ], 1, function(x, t) lines(t, x), t = t))
```

Evolution proceeds along a random walk regardless of σ^2^, with some replicates of the process evolving toward higher values for the phenotype and others evolving lower. However, variation among simulations accrues at a much lower rate (i.e., σ^2^ * time).

```{r chunk4}
# calculate variance of columns
v <- apply(x, 2, var)
# plot the results
par(mar = c(5.1, 4.1, 2.1, 2.1))
plot(t, v,
    ylim = c(0, 0.1), type = "l", xlab = "time",
    ylab = "variance", bty = "n", las = 1,
    cex.axis = 0.8
)
lines(t, t * sig2 / 10, lwd = 3, col = rgb(0, 0, 0, 0.1))
legend("topleft", c("observed variance", "expected variance"),
    lwd = c(1, 3), col = c("black", rgb(0, 0, 0, 0.1)),
    bty = "n", cex = 0.8
)
```

The accumulation of variation among simulations more or less follows a straight line in which the slope of the line is approximately equal to the value of σ^2^ that we used for the simulations.

The variance at the end of the simulation should just be σ^2^ (0.001, in our case) multiplied by the total time elapsed (100), or about 0.1.
```{r chunk5}
# find variance at the end of the simulations
var(x[, length(t)])
```
###  Brownian motion on a phylogeny

We can simulate Brownian motion evolution from the root up the branches of a phylogenetic tree.

```{r chunk6}
## simulate a tree and Brownian evolution on that
## tree using simBMphylo
object <- simBMphylo(
    n = 6, t = 100, sig2 = 0.01,
    fsize = 0.8, cex.axis = 0.6, cex.lab = 0.8,
    las = 1
)
```

Variation between lineages increases through time and, because lineages can only begin to differentiate after they’ve diverged, the amount of variation that accrues between species appears to increase as a function of the amount of evolutionary time since they shared common ancestry.

```{r chunk7}
## pull the phylogeny out of the object we simulated
## for figure 4.5 using simBMphylo
tree <- object$tree
## simulate 1000 instance of Brownian evolution on that
## tree
x <- fastBM(tree, nsim = 1000)
```

If the scatter of points for a hypothetical pair of species i and j is tightly clustered to the 1:1 line, then this indicates that (across many replicates of the evolutionary process) species i and j always tend to evolve similar values for the trait. 

By contrast, if the scatter of points for species i and j is diffuse and uncorrelated, this indicates that there is no tendency in our simulation for species i and j to evolve similar values of the trait across many instances of Brownian evolution on our tree.

```{r chunk8}
## set the orientation of the axis labels to be
## horizontal
par(las = 1)
## create a scatterplot matrix from our simulated
## data using pairs
```
```{r chunk9}
pairs(t(x)[, tree$tip.label[6:1]],
    pch = 19,
    col = make.transparent("blue", 0.05),
    cex.axis = 0.9
)
```

As expected, the trait values of closely related species (such as C and D) tend to be more highly correlated across simulations than do the trait values of distant taxa. 

Furthermore, species whose MRCA is also the global root of the tree have no correlation at all!

### Fitting a Brownian model to data

Just as we can simulate data on a phylogeny under a Brownian motion evolution model, we can also fit a Brownian model to our data and tree.

The Brownian model has two different parameters that we can estimate.

σ^2^: “instantaneous variance” of the stochastic evolutionary process under Brownian motion (i.e., the "evolutionary rate")
x_0_: initial state of the process (i.e., ancestral state at the root node of our tree)

#### Maximum likelihood estimation

We need a criterion for choosing values of σ^2^ and x_0_ that best fit our data.

One strategy is to select the values that maximize the probability of obtaining the data that we’ve observed. This strategy is called estimation by maximum likelihood.

Maximum likelihood estimators have been shown to possess many desirable statistical properties, such as consistency, efficiency, and asymptotic unbiasedness.

We will use this bacterial genome size dataset to explore the application of a Brownian motion model using maximum likelihood.

```{r chunk10.1}
## read bacterial data from file
bacteria.data <- read.csv("bac_rates.csv", row.names = 1)
head(bacteria.data, 3)
```


```{r chunk10.2}
## read bacterial data tree from file
bacteria.tree <- read.tree("bac_rates.phy")
print(bacteria.tree, printlen = 2)
```

```{r chunk10.3}
name.check(bacteria.tree, bacteria.data)
genome_size <- bacteria.data[, "Genome_Size_Mb"]
genome_size
```

```{r chunk11}
names(genome_size) <- rownames(bacteria.data)
head(genome_size)
```

```{r chunk12}
bacteria.tree <- read.tree("bac_rates.phy")
print(bacteria.tree, printlen = 2)
```

```{r chunk13}
## graph phylogeny using plotTree
plotTree(bacteria.tree,
    ftype = "i", fsize = 0.5,
    lwd = 1, mar = c(2.1, 2.1, 0.1, 1.1)
)
## add a horizontal axis to our plot
axis(1, at = seq(0, 1, length.out = 5), cex.axis = 0.8)
```

And check that the tips all have character data:

```{r chunk14}
name.check(bacteria.tree, bacteria.data)
```

The *fitContinuous* function takes a single trait vector as input. We will extract a single trait (genome size) from the data frame.

```{r chunk14.2}
genome_size <- bacteria.data[, "Genome_Size_Mb"]
genome_size
```

The vector is missing the species names, which will not work. We need to re-inject those names into this dataset.

```{r chunk14.3}
names(genome_size) <- rownames(bacteria.data)
head(genome_size)
```

*fitContinuous*, as its name suggests, fits a variety of different continuous character models to data and a phylogeny.

The default model is Brownian motion.

```{r chunk15}
## fit Brownian motion model using fitContinuous
fitBM_gs <- fitContinuous(bacteria.tree, genome_size)
fitBM_gs
```

From the summary of our fitted model, we see that the maximum likelihood estimate (MLE) of σ^2^ is about 25.0 and that the MLE of the root state x_0_ (here given as z_0_) is around 1.98.

Since our genome sizes have been measured in Megabases (Mb), a σ^2^ of 25 indicates that under a Brownian process, we’d expect a large number of independently evolving lineages to accumulate the variance among each other of 25 Mb after one unit of time.

The MLE of x_0_ of 1.98 means that the most likely state at the root of the tree, under our model, is a genome size of 1.98 Mb.

*fitContinuous* also reports some information about the optimization process. *fitContinuous* uses numerical optimization, and in this case, it tells us how many independent optimization iterations were used (optimization iterations) and how frequently optimization converged on the same, best solution.

Brownian motion is such a simple model that most often this latter quantity will be 1.00, indicating that the optimizer found the same optimal solution in 100% of iterations. For more complex models, the fraction will often be lower than 100%. A fraction lower than 100% should not concern us until the number of iterations that converge on the same best solution (that is, optimization iterations × frequency of best fit) falls to a very low value. In that case, we might have good cause to really worry that our optimizer failed to find the true MLEs of our model parameters!

Let’s do the same analysis—but this time with the mutation accumulation rate.

```{r chunk16}
## pull our mutation accumulation rate as a named vector
mutation <- setNames(
    bacteria.data[, "Accumulation_Rate"],
    rownames(bacteria.data)
)
head(mutation)
```

If we plot the distribution of mutation accumulation rate across our different species, we’ll see that it is highly left-skewed.

The very highly skewed distribution that we see for this trait is a strong signal that Brownian evolution is very likely a bad fit to this trait on its original scale. Let’s transform mutation accumulation rate to a log-scale and plot.

Note: we don’t necessarily expect a precisely normal distribution of trait values under Brownian evolution, but strong skew can indicate that Brownian motion is not appropriate. For instance, if our tree consists of two deeply divergent clades, each consisting of a large number of closely related species, then the most likely distribution of trait values under Brownian motion is bimodal — one mode for each clade.

```{r chunk17}
## set up for side-by-side plots
par(mfrow = c(1, 2), mar = c(6.1, 4.1, 2.1, 1.1))
## histogram of mutation accumulation rates on original scale
hist(mutation,
    main = "", las = 2, xlab = "",
    cex.axis = 0.7, cex.lab = 0.9,
    breaks = seq(min(mutation), max(mutation),
        length.out = 12
    )
)
mtext("(a)", adj = 0, line = 1)
mtext("rate", side = 1, line = 4, cex = 0.9)
## histogram of mutation accumulation rates on log scale
ln_mutation <- log(mutation)
hist(ln_mutation,
    main = "", las = 2, xlab = "",
    cex.axis = 0.7, cex.lab = 0.9,
    breaks = seq(min(ln_mutation), max(ln_mutation),
        length.out = 12
    )
)
mtext("(b)", adj = 0, line = 1)
mtext("ln(rate)", side = 1, line = 4, cex = 0.9)
```

It seems clear that it’s going to be better to work with mutation accumulation rate on a log-scale rather than on its original scale.

```{r chunk18}
## fit Brownian motion model to log(mutation accumulation)
fitBM_ar <- fitContinuous(bacteria.tree, ln_mutation)
fitBM_ar
```

You may be tempted to compare the rate parameter estimate for mutation accumulation rate with our estimate of σ^2^ for genome size — but, keep in mind, the former is in units of log(rate)^2^, while the latter is in Mb.

### Phylogenetic Signal

Something that we haven’t mentioned so far in this book is the popular concept of phylogenetic signal, which we’d define as the tendency for related species to resemble one another more than expected by chance (Revell et al. 2008).

Phylogenetic signal, is very closely related to Brownian motion because the most popular ways of measuring signal do so with reference to this model. That is, they ask whether species tend to resemble each other less or more than one would expect based on a Brownian model of evolutionary change through time.

We’ll examine two different methods for measuring phylogenetic signal of quantitative characters
1. Blomberg et al.’s (2003) **K**
2. Pagel’s (1999a) **λ**

#### Blomberg et al.’s **K**

Blomberg et al.’s (2003) **K** is best summarized as a normalized ratio comparing the variance *among* clades on the tree to the variance *within* clades.

If the variance among clades is high (compared to the variance within clades), then phylogenetic signal is said to be high.

Conversely, if the variance within clades is high (compared to the variance among clades), then phylogenetic signal will be low.

This ratio is then normalized by dividing it by its expected value under a Brownian evolutionary process. As such, Blomberg et al.’s (2003) **K** has an expected value of 1.0 under evolution by Brownian motion.

```{r chunk19}
phylosig(bacteria.tree, genome_size)
```

In this case, we see that our measure of phylogenetic signal, **K**, for genome size has a value of around 0.35, which is lower than we’d expect under evolution by Brownian motion.

#### Testing hypotheses about Blomberg et al.’s **K**

What does **K < 1** mean? 

We can test whether the amount of phylogenetic signal in our data (by whatever measure) exceeds the quantity of signal expected by random chance.

One way to do that is by simply randomizing the data across the tips of the tree a large number of times and then repeatedly recalculating phylogenetic signal by the same measure for each randomized data set.

The fraction of randomizations with equal or higher values of phylogenetic signal than our observed value is our P-value for a null hypothesis test of no signal.

```{r chunk20}
## test for significant phylogenetic signal using
## Blomberg’s K
K_gs <- phylosig(bacteria.tree, genome_size,
    test = TRUE, nsim = 10000
)
K_gs
```

Null distribution of **K** that we generated using randomization for a hypothesis test of phylogenetic signal in bacterial genome size. The observed value of **K** is indicated by the arrow.

```{r chunk21}
## set plot margins and font size
par(cex = 0.8, mar = c(5.1, 4.1, 2.1, 2.1))
## plot null-distribution and observed value of K
plot(K_gs, las = 1, cex.axis = 0.9)
```

This result, tells us that despite being considerably less than 1.0, our observed value of phylogenetic signal, **K**, is still larger than the value of **K** we’d expect to find if our data for genome size were random with respect to the phylogeny.

Now let’s repeat the same exercise, but this time using mutation accumulation rate.

```{r chunk22}
## test for phylogenetic signal in mutation accumulation
## rate
K_ar <- phylosig(bacteria.tree, ln_mutation,
    test = TRUE, nsim = 10000
)
K_ar
```

```{r chunk23}
## plot the results
par(cex = 0.8, mar = c(5.1, 4.1, 2.1, 2.1))
plot(K_ar, las = 1, cex.axis = 0.9)
```

Our phylogenetic signal is again very low, but this time we can’t reject the null hypothesis. This means that our observed value of **K** for mutation accumulation rate (on a log-scale) is entirely consistent with what we’d expected to obtain by chance if species mutation accumulation rates were randomly arrayed on the phylogeny.

But are these values of phylogenetic signal inconsistent with the Brownian Motion model (or any other model)? To test this, we’ll have to generate a null distribution of values for **K** under our null hypothesis of Brownian motion.

```{r chunk24}
## simulate 10000 datasets
nullx <- fastBM(bacteria.tree, nsim = 10000)
## for each, carry out a test for phylogenetic signal
## and accumulate these into a vector using sapply
nullK <- apply(nullx, 2, phylosig, tree = bacteria.tree)
## calculate P-values
Pval_gs <- mean(nullK <= K_gs$K)
Pval_gs
```

```{r chunk25}
Pval_ar <- mean(nullK <= K_ar$K)
Pval_ar
```

We first generated 10,000 data sets under a Brownian model by simulating evolution on our mammal tree using *fastBM*. We then computed a value of **K** for each simulated data vector by iterating across the columns of our simulated data using *apply*.

Finally, we compute a P-value by counting the proportion of times our simulated values of **K** were smaller than our observed values.

Note: We only counted the fraction of times they were smaller because our observed **K** values are both less than 1. If they were above 1, we could count the number of simulated data sets with larger values than our observed value. For a two-tailed test, we would need to multiply this count by 2.

What we find is that only the phylogenetic signal for mutation accumulation rate is significantly lower than we’d expect under Brownian evolution. For genome size, we often obtain signal values that are as small as the observed data, even when data are simulated under Brownian motion.

We can also visualize these null distributions:

```{r chunk26}
## set up for side-by-side plots
par(mfrow = c(1, 2))
## plot for Genome size
## null distribution
hist(c(nullK, K_gs$K),
    breaks = 30, col = "lightgray",
    border = "lightgray", main = "", xlab = "K", las = 1,
    cex.axis = 0.7, cex.lab = 0.9, ylim = c(0, 4000)
)
## actual value as an arrow
arrows(
    x0 = K_gs$K, y0 = par()$usr[4], y1 = 0, length = 0.12,
    col = make.transparent("blue", 0.5), lwd = 2
)
text(K_gs$K, 0.96 * par()$usr[4],
    paste("observed value of K (P = ",
        round(Pval_gs, 4), ")",
        sep = ""
    ),
    pos = 4, cex = 0.8
)
mtext("(a)", line = 1, adj = 0)
## plot for mutation accumulation rate
## null distribution
hist(c(nullK, K_ar$K),
    breaks = 30, col = "lightgray",
    border = "lightgray", main = "", xlab = "K", las = 1,
    cex.axis = 0.7, cex.lab = 0.9, ylim = c(0, 4000)
)
## actual value as an arrow
arrows(
    x0 = K_ar$K, y0 = par()$usr[4], y1 = 0, length = 0.12,
    col = make.transparent("blue", 0.5), lwd = 2
)
text(K_ar$K, 0.96 * par()$usr[4],
    paste("observed value of K (P = ",
        round(Pval_ar, 4), ")",
        sep = ""
    ),
    pos = 4, cex = 0.8
)
mtext("(b)", line = 1, adj = 0)
```

Null distributions for **K** under the null hypothesis of Brownian motion. (a) Genome size. (b) Mutation accumulation rate.

#### Pagel’s **λ**

In addition to Blomberg et al.’s **K**, another popular measure of phylogenetic signal is called Pagel’s **λ**.

**λ** is a scaling coefficient for the off-diagonal elements in the expected correlations among species that we learned about in **Chapter 3** (Pagel 1999a).

Values of **λ** < 1 correspond to less phylogenetic signal than expected under a Brownian motion model. Unlike **K**, however, **λ** is not generally well defined outside of the range of (0,1). As such, **λ** is more appropriate for detecting phylogenetic signal that is *lower* than expected under Brownian motion than the converse (i.e., characters that poorly reflect the evolutionary process).

We can calculate Pagel’s **λ** for the bacterial dataset.

```{r chunk27}
## compute phylogenetic signal, lambda, for genome size
## and mutation accumulation rate
phylosig(bacteria.tree, genome_size, method = "lambda")
```

```{r chunk28}
phylosig(bacteria.tree, ln_mutation, method = "lambda")
```

Both traits have **λ** estimates that are close to 1; however, **λ** is higher for genome size than for mutation accumulation rate (*i.e.*, both exhibit similar amount of phylogenetic signal as expected under Brownian Motion, however mutation accumulation does to a lesser degree).

This is similar to our findings for **K**, in which we showed that genome size had phylogenetic signal similar to the expectation under Brownian motion, while mutation accumulation rate had less.

However, we do not expect a one-to-one correspondence between **K** and **λ**. The two metrics actually measure different aspects of phylogenetic signal (also see Boettiger et al. 2012 for additional commentary on **λ** estimation).

The **λ** method also allows for a hypothesis test of a null that **λ = 0**. Since **λ** is estimated using likelihood, this can be done most easily using a likelihood ratio test.

Here is what that looks like, also plotting the likelihood surface for each trait.

```{r chunk29}
## test for significant phylogenetic signal, lambda,
## in each of our two traits
lambda_gs <- phylosig(bacteria.tree, genome_size,
    method = "lambda", test = TRUE
)
lambda_gs
```

```{r chunk30}
lambda_ar <- phylosig(
    tree = bacteria.tree, x = ln_mutation,
    method = "lambda", test = TRUE
)
lambda_ar
```

```{r chunk31}
## plot the likelihood surfaces
## first set plotting parameters, including subdividing
## our plot area into 1 column and two rows
par(
    mfrow = c(2, 1), mar = c(5.1, 4.1, 2.1, 2.1),
    cex = 0.8
)
## plot the likelihood surfaces of lambda for each of our
## two traits
plot(lambda_gs,
    las = 1, cex.axis = 0.9, bty = "n",
    xlim = c(0, 1.1)
)
mtext("(a)", line = 1, adj = 0)
plot(lambda_ar,
    las = 1, cex.axis = 0.9, bty = "n",
    xlim = c(0, 1.1)
)
mtext("(b)", line = 1, adj = 0)
```

This tells us that we can reject a null hypothesis of **λ = 0** for both phenotypic traits in our data set.

It’s possible to test a null hypothesis of **λ = 1**. In this case, however, we will take advantage of the fact that, in the case of *method="lambda"*, *phylosig* exports the likelihood function that it used for optimization.

As such, we can just compute the likelihood for **λ = 1** and then compare it to our MLE of **λ** using a likelihood ratio test with 1 degree of freedom.

```{r chunk32}
LR_gs <- -2 * (lambda_gs$lik(1) - lambda_gs$logL)
LR_gs
```

*pchisq* (for *lower.tail=FALSE*) gives us the probability of observing an equally or more extreme value of our likelihood ratio than the one we calculated for each trait, assuming that the likelihood ratio is χ^2^-distributed under the null.

```{r chunk33}
Pval_lambda_gs <- pchisq(LR_gs,
    df = 1,
    lower.tail = FALSE
)
Pval_lambda_gs
```

This tells is that we cannot reject a null hypothesis of **λ = 1** for genome size. 

Now, let’s repeat the same thing for mutation accumulation rate.

```{r chunk34}
LR_ar <- -2 * (lambda_ar$lik(1) -
    lambda_ar$logL)
Pval_lambda_ar <- pchisq(LR_ar,
    df = 1,
    lower.tail = FALSE
)
Pval_lambda_ar
```

Interestingly, and consistent with what we saw with Blomberg’s **K**, we’re only able to reject our null hypothesis of **λ = 1** for mutation accumulation rate.

### Other models of continuous character evolution on phylogenies

There are other popular models for continuous character evolution on phylogenies, including:
1. "Early Burst" (EB) (Blomberg et al. 2003)
2. Ornstein-Uhlenbeck (OU) (Hansen 1997; see also Butler & King 2004)

#### Early Burst (EB) Model

Under the EB model, the rate of evolution, σ^2^, starts with some initial value at the root of the tree and then declines monotonically through time according to an exponential decay function.

We can plot the rate of evolution through time under conditions of an initial rate of evolution (σ_0_^2^) of 1.0 and an exponential decay parameter (*a*) of -0.04.

```{r chunk35}
## set parameters of the EB process
sig2.0 <- 1.0
a <- 0.04
t <- 100
## compute sigmaˆ2 as a function of time under this
## process
sig2.t <- sig2.0 * exp(-0.04 * 0:t)
## graph sigmaˆ2 through time
par(mar = c(5.1, 4.1, 2.1, 2.1))
plot(0:t, sig2.t,
    type = "l", xlab = "time",
    ylab = expression(sigma^2), bty = "l",
    las = 1, cex.axis = 0.9
)
```
The rate of evolution through time under an EB model with a = -0.04.

The effect of this type of character evolution results in a declining rate of evolution through time that will tend to result in large differences between clades and relatively small differences within them.

We can also graph evolutionary change through time on a phylogeny under *EB* using *simBMphylo*

```{r chunk36}
## visualize early-burst evolution using simBMphylo
## we send only the first 100 elements of sig2.t to the function
## because 0:100 produces 101 values but our tree only
## contains 100 units of time from the root to any tip
object <- simBMphylo(6, 100,
    sig2 = sig2.t[1:100],
    fsize = 0.8, cex.axis = 0.6, cex.lab = 0.8,
    las = 1
)
```
EB evolution on a simulated phylogeny.

The EB model is one in which differences between lineages tend to accumulate rapidly near the beginning of a diversification. As such, it has often been linked to the concept of adaptive radiation.

#### The Ornstein–Uhlenbeck (OU) model

This model is a relatively simple extension of Brownian motion, with an additional parameter (α) that describes the tendency to return toward a particular central value (θ).

The Ornstein–Uhlenbeck model is most often interpreted as a model for adaptive evolution in which α corresponds to the strength of natural selection and θ to the position of the optimum.

θ can be thought of as an "attractor" that species are under selection to fit and α is the "attractive force" of the attractor. 

### Fitting and comparative alternative continuous character models

We can fit all three models to our data and compare their fits using likelihood.

We can specify a value for the argument *model* in the *geiger* function *fitContinuous*, starting with *model="EB"*.

```{r chunk37}
## fit EB model to genome size
fitEB_gs <- fitContinuous(bacteria.tree, genome_size,
    model = "EB"
)
```

```{r chunk38}
fitEB_gs
```

We obtain the error: ```In fitContinuous (bacteria.tree, genome_size, model = "EB") : Parameter estimates appear at bounds: a```

Examining the model output, uur ML estimated value of the decay parameter, a, is almost exactly zero.

When a = 0.0, the EB model reduces to a constant rate Brownian motion. This is not an error! It merely indicates that when we impose an EB model on our data, the best-fitting EB model is Brownian evolution!

We can fit an OU model using *model="OU"*.

```{r chunk39}
## fit OU model to genome size
fitOU_gs <- fitContinuous(bacteria.tree, genome_size,
    model = "OU"
)
```

```{r chunk40}
fitOU_gs
```

We see a highly similar error message, but this time for the parameter α (*alpha* in the model object) for the OU model.

In this case, our best-fitting OU model is not a Brownian model (which would correspond to α = 0.0); α is quite different from zero, in fact.

The warning message we received is very important because it indicates not that the MLE of α is near 0 (which would be equivalent to a Brownian motion model) but that it is at the default upper bound for optimization.

Let’s change these default bounds with *bounds = list(alpha = c(0, 10)* and see what result we obtain.

```{r chunk41}
fitOU_gs <- fitContinuous(bacteria.tree, genome_size,
    model = "OU", bounds = list(alpha = c(0, 10))
)
fitOU_gs
```

There’s no warning message—and both our optimized value of α and our log-likelihood are higher, which makes sense.

To compare either EB and Brownian *or* OU and Brownian, we could use a likelihood ratio test. However, to compare all three models ***simultaneously***, our best option is probably to use an information criterion such as the AIC.

```{r chunk42}
## accumulate AIC scores from our three models into
## a vector
aic_gs <- setNames(
    c(
        AIC(fitBM_gs),
        AIC(fitEB_gs), AIC(fitOU_gs)
    ),
    c("BM", "EB", "OU")
)
aic_gs
```

The preferred model under this criterion should be the one with the lowest AIC.

This result tells us that the best-supported model (among those tested) for genome size is Ornstein–Uhlenbeck.

We can also compute Akaike weights using the *phytools* function *aic.w*.

```{r chunk43}
aic.w(aic_gs)
```

The overwhelming majority of weight falls on the OU model—but evidence in support of the different models is somewhat split, and there is also quite a bit of weight on Brownian motion.

Let’s now repeat all of this analysis, but for mutation accumulation rate.

```{r chunk44}
## fit EB model
fitEB_ar <- fitContinuous(bacteria.tree, ln_mutation,
    model = "EB"
)
## fit OU model
fitOU_ar <- fitContinuous(bacteria.tree, ln_mutation,
    model = "OU", bounds = list(alpha = c(0, 100))
)
## accumulate AIC scores in a vector
aic_ar <- setNames(
    c(
        AIC(fitBM_ar),
        AIC(fitEB_ar), AIC(fitOU_ar)
    ),
    c("BM", "EB", "OU")
)
## compute and print Akaike weights
aic.w(aic_ar)
```

Almost 100 percent of the weight of support falls on the OU model, with virtually none at all on the Brownian or EB models.

```{r chunk45}
fitOU_ar
```

This tells us that a stochastic process with a tendency to revert toward a central value is better at explaining our data for mutation accumulation rates than BM or EB.

This is actually quite a common pattern across comparative data sets (Harmon et al. 2010).

Question: does the commonality of OU come from the combination of:
1. Constraints on character trait values
2. Selection on character traits values

## Practice Problems

### 4.1

The data set used in this chapter also includes estimates of GC content ("GC_Content_Percent"). Fit the three models (BM, OU, and EB) for this character. Also test for phylogenetic signal in GC content. What do you conclude?

### 4.2

Reanalyze the data for mutation accumulation rate, but this time do not log-transform the data. What happens? Can you explain the discrepancy?

### 4.3

You might wonder about the relationship between the models considered here and phylogenetic signal. Use a simulation study to find out! Focus for now on the OU model, which can be simulated using fastBM in phytools. For simplicity, you can use the bacterial tree, bacteria.tree, that we used in this exercise. On that tree, simulate OU characters with σ^2^= 1, expected mean θ = 0, and root state a = 0. Vary α over a range of values from small α = 0.1 to large α = 10. For each simulation, determine the amount of phylogenetic signal, and do a significance test. See if you can detect the general pattern!
