## Chapter 6: Modeling discrete character evolution on a phylogeny

### Prepare for analyses

```{r chunk0}
library(phytools)
set.seed(1234)
```


Many traits, such as the presence or absence of a feature, can only be recorded in a discontinuous or discrete fashion. Others, such as color or habitat type, could be continuous in theory but most often (or most conveniently) will be discretely coded by investigators.

In this chapter we:

1. Use simulation to introduce the Mk model, a model for the evolution of discrete characters on trees.
2. Show how to fit the Mk model to data and how to estimate model parameters.
3. Fit three different, commonly used discrete models, called the equal-rates, symmetric, and all-rates-different models, and show how to compare among models using likelihood and Akaike information criterion (AIC).
4. Describe how to implement a custom Mk model using a design matrix.
5. Apply all of these approaches to analyze the evolution of digit number in squamates.

### The M*k* model

The dominant model for the evolution of discrete characters on phylogenies is a model that has been named the M*k* model (Lewis 2001). 

The **M*k*** model describes a continuous-time, discrete ***k***-state **M**arkov process. This model was inspired directly by models of sequence evolution (e.g., those in Yang 2006).

In the M*k* process:
- Changes can occur between states at any time
- The rate of change to other states depends only on the current state (not on prior states of duration of time in current state)
    - We say the process has "no memory" (i.e., it is a "Markovian" processes)
    - All that matters is the current state and the rate of change to other states
- The waiting times between changes will be exponentially distributed
    - The exponential shape parameter depends only on the rate of change between states, typically denoted *q* (Yang 2006)
    - Large *q* = fast rate; waiting time between changes (1/*q*) will tend to be short
    - Small *q* = slow rate; waiting times between changes (1/*q*) will tend to be long

Initially, the M*k* model was defined with one transition rate between all states. Now there variants (termed the "Extended M*k* Model") allow for differing rates.

In the general case, we use a *k*x*k* matrix ("**Q** Matrix") to represent all the different rates of change between states.

Each element of the **Q** matrix (***Q***~*i*,*j*~) gives the instantaneous rate of change between the states *i* and *j*.

#### Simulating character histories under the Mk model

The *phytools* function `sim.history` is one function that can be used to simulate data under the Mk model. 

`sim.history` simulates the entire history of a character on the tree — not just the end state at the tips — and we’ll use it to simulate data for a binary (0/1) character under three different evolutionary scenarios.

Note: *phytools* also contains `sim.Mk` which is much faster, but only simulates tip-states, not the full character history 

```{r chunk1}
############################################################
## load phytools
library(phytools)
## assign colors for the two states
colors <- setNames(c("gray", "black"), 0:1)
############################################################
## simulate a stochastic pure-birth tree with 100 taxa
tree <- pbtree(n = 100, scale = 1)
## divide plotting area into three panels
par(mfrow = c(1, 3))
## Q matrix for simulation 1: equal backward & forward
## rates (ER)
Q1 <- matrix(c(-1, 1, 1, -1), 2, 2,
    byrow = TRUE, dimnames = list(0:1, 0:1)
)
Q1
############################################################
## simulate ER character evolution and plot
plot(sim.history(tree, Q1, message = FALSE),
    colors,
    ftype = "off", mar = c(1.1, 1.1, 1.1, 0.1)
)
mtext("(a)", line = -1, adj = 0)
legend(
    x = "bottomleft", legend = c("0", "1"),
    pt.cex = 1.5, pch = 15, col = colors,
    bty = "n"
)
## Q matrix for simulation 2: different backward &
## forward transition rates (ARD)
Q2 <- matrix(c(-1, 1, 0.25, -25), 2, 2,
    byrow = TRUE, dimnames = list(0:1, 0:1)
)
Q2
############################################################
## simulate ARD character evolution and plot
plot(sim.history(tree, Q2,
    direction = "row_to_column", message = FALSE
),
colors,
ftype = "off", mar = c(1.1, 1.1, 1.1, 0.1)
)
mtext("(b)", line = -1, adj = 0)
## Q matrix for (effectively) irreversible trait
## evolution (changes from 1->0, but not the reverse)
## note: values cannot be zero, but they can be very, very small
Q3 <- matrix(c(-1e-12, 1e-12, 1, -1), 2, 2,
    byrow = TRUE, dimnames = list(0:1, 0:1)
)
Q3
############################################################
## simulate irreversible character evolution and plot
plot(sim.history(tree, Q3,
    anc = "1",
    direction = "row_to_column",
    message = FALSE
), colors,
ftype = "off",
mar = c(1.1, 1.1, 1.1, 0.1)
)
mtext("(c)", line = -1, adj = 0)
```

We simulated 4 scenarios (black = 1, grey = 0):

1. Equal rate of change between all states
    0: 62
    1: 38
    0->1: 23
    1->0: 14
    total changes: 37
2. Higher (4x) rate of change from state 0 -> 1 than 1 -> 0
    0: 43
    1: 57
    0->1: 18
    1->0: 4
    total changes: 22
3. Irreversible rate of change from state 0 -> 1
    0: 18
    1: 82
    0->1: 6
    1->0: 0
    total changes: 6

These **Q** matrices lead to the following tip states:
1. Even distribution of states (...not really though)
2. More taxa in state of 1
3. Entire clades are fixed for the state of 1

QUESTION: How is the root state inferred? 50/50? Worked backwards from the tips?
QUESTION: Are the specific values of *q* important? Or just their ratios among states? If they are important, how can we select them?

#### Fitting the Mk model to data

For any particular transition matrix, **Q**, we can compute its likelihood as the probability of a pattern of character states at the tips of the tree. 

If we proceed to find the value of **Q** that maximizes this probability, we’ll have found the maximum likelihood estimate (MLE) of **Q** (Yang 2006).

The example phylogeny is a large tree of snakes and lizards, and the data consist of the number of digits (toes) in the hindfoot of these animals (Brandley et al. 2008).

```{r chunk2}
library(geiger)
## read data matrix
sqData <- read.csv("squamate-data.csv", row.names = 1)
## print dimensions of our data frame
dim(sqData)
## read phylogenetic tree
sqTree <- read.nexus("squamate.tre")
print(sqTree, printlen = 2)
## plot our tree
plotTree(sqTree, type = "fan", lwd = 1, fsize = 0.3, ftype = "i")
```

Check correspondence between trait and phylogenetic data.

```{r chunk3}
## check name matching
chk <- name.check(sqTree, sqData)
summary(chk)
## drop tips of tree that are missing from data matrix
sqTree.pruned <- drop.tip(sqTree, chk$tree_not_data)
## drop rows of matrix that are missing from tree
sqData.pruned <- sqData[!(rownames(sqData) %in%
    chk$data_not_tree), , drop = FALSE]
```

```{r chunk4}
## extract discrete trait
toes <- setNames(
    as.factor(sqData.pruned[, "rear.toes"]),
    rownames(sqData.pruned)
)
head(toes)
```

We’ll use the function `fitDiscrete` in *geiger* to model the **Q** matrix with the highest likelihood.

Other functions will fit an M*k* model (like `fitMk` in *phytools*). The main difference between `fitContinuous` and `fitMk` is the assumption that each function makes about the state at the root node of the tree.

In most cases, this difference will be of little consequence, but occasionally, it can affect the inferred model to a larger extent. We recommend thinking carefully about this assumption rather than taking it for granted.

QUESTION: How can we know which assumption is better?

#### The equal-rates (ER) model

The “equal-rates” or ER model fits a single transition rate between all pairs of states for our discrete trait (Harmon 2019).

```{r chunk5}
## fit ER model to squamate toe data using fitDiscrete
fitER <- fitDiscrete(sqTree.pruned, toes, model = "ER")
print(fitER, digits = 3)
```

This yields the parameter estimate, log-likelihood AIC, and convergence diagnostics for our ER fit.

For an ER model, there is just one model parameter, the transition rate, *q*. We estimate *q*= 0.00103. The expected number of transitions given a particular amount of time, *t*, can be calculated as the simple product of *q* and *t* (Yang 2006).

```{r chunk6}
## plot fitted ER model
plot(fitER, mar = rep(0, 4), signif = 5)
```

This is a graph of the transition rate model.

#### The symmetric transition model (SYM)

Another popular discrete character model is a model called the “symmetric rates” model, SYM (Harmon 2019).

This model assumes that the rate of transition from each character state *i* to each state *j* is equal to the rate of change from state *j* to state *i* but that each pair of character states can have a different rate.

```{r chunk7}
## fit SYM model
fitSYM <- fitDiscrete(sqTree.pruned, toes, model = "SYM")
print(fitSYM, digits = 3)
## graph fitted SYM model
## show.zeros=FALSE removes arrows of rates very close to 0
plot(fitSYM, show.zeros = FALSE, mar = rep(0, 4), signif = 5)
```

#### All-rates-different model

Within the extended Mk framework, the most complicated model imaginable* is one in which every type of transition is allowed to occur with a different rate.

This is called the “all-rates-different” model or ARD (Harmon 2019).

*: assuming a constant process across all the branches and nodes of the phylogeny (but see Chapter 7)

```{r chunk8}
## fit ARD model
fitARD <- fitDiscrete(sqTree.pruned, toes, model = "ARD")
print(fitARD, digits = 3)
## plot fitted model
plot(fitARD, show.zeros = FALSE, mar = rep(0, 4), signif = 5)
```

#### Custom transition models: An ordered evolution model

ER, SYM, and ARD are the most common models for discrete character evolution. However, they do not comprise the complete range of possible models we could imagine fitting to the evolution of a discrete character trait on a tree.

For the example dataset, we might hypothesize that toes can be lost and not reacquired, or perhaps that toes can be lost and regained, but that only changes between adjacent states (e.g., from five to four, from two to three, and so on) can occur (Brandley et al. 2008).

`fitDiscrete` (and `phytools::fitMk`) gives us the flexibility to specify this kind of model. The way we need to do it is by first creating a *design matrix*.

Each position in the matrix with a nonzero positive integer indicates a type of change that can occur, and any cells in the matrix with the same integer will have the same rate of change. We put zeros on the diagonal of our matrix, as well as in cells of the matrix that correspond to changes that are not permitted to occur.

The integers in this matrix just correspond to the different parameters that we want R to estimate from our data — and their relative values thus indicate nothing at all about the values of those parameters.

**Ordered Model**: changes are permitted between all pairs of adjacent states and in which we will allow these changes to occur at different rates.

**Directional Model**: digits can be lost but cannot be reacquired and every different type of digit loss (i.e., from five to four digits, from four to three digits, etc.) is allowed to proceed at a different rate.

```{r chunk9}
## create design matrix for bi-directional
## ordered model
ordered.model <- matrix(c(
    0, 1, 0, 0, 0, 0,
    2, 0, 3, 0, 0, 0,
    0, 4, 0, 5, 0, 0,
    0, 0, 6, 0, 7, 0,
    0, 0, 0, 8, 0, 9,
    0, 0, 0, 0, 10, 0
), 6, 6,
byrow = TRUE,
dimnames = list(0:5, 0:5)
)
ordered.model
## create design matrix for directional ordered
## model
directional.model <- matrix(c(
    0, 0, 0, 0, 0, 0,
    1, 0, 0, 0, 0, 0,
    0, 2, 0, 0, 0, 0,
    0, 0, 3, 0, 0, 0,
    0, 0, 0, 4, 0, 0,
    0, 0, 0, 0, 5, 0
), 6, 6,
byrow = TRUE,
dimnames = list(0:5, 0:5)
)
directional.model
```

You can count the number of parameters in a design matrix by counting the number of unique positive integers. 

Fitting our ordered model will result in a total of **ten** estimated parameters

Fitting our directional model will result in the estimation of only **five** parameters.

```{r chunk10}
## fit bi-directional ordered model
fitOrdered <- fitDiscrete(sqTree.pruned, toes,
    model = ordered.model, surpressWarnings = TRUE
)
print(fitOrdered, digits = 3)
## fit directional (loss only) ordered model
fitDirectional <- fitDiscrete(sqTree.pruned, toes,
    model = directional.model, surpressWarnings = TRUE
)
print(fitDirectional, digits = 3)
## split plot area into two panels
par(mfrow = c(1, 2))
## plot ordered and directional models
plot(fitOrdered,
    show.zeros = FALSE, signif = 5,
    mar = c(0.1, 1.1, 0.1, 0.1)
)
mtext("(a)", line = -2, adj = 0, cex = 1.5)
plot(fitDirectional,
    show.zeros = FALSE, signif = 5,
    mar = c(0.1, 1.1, 0.1, 0.1)
)
mtext("(b)", line = -2, adj = 0, cex = 1.5)
```

The ordered model allows toe number to increase or decrease by one step at a time.

The directional model does not allow for decreases in toe number.

The number of different models we can fit to discrete character data is virtually limited only by the bounds of our imagination; however, as a general rule, we typically recommend fitting models that are relatively simple, biologically justifiable, or (ideally) both.

#### Comparing alternative discrete character models

We can compare alternative models (and identify the model in our set that is best supported by our data) using a likelihood ratio test.

According to the theory of likelihoods, two times the difference in log-likelihoods of **nested models*** should be distributed as a χ^2^ with degrees of freedom equal to the difference in the number of parameters between the two fitted models (Wilks 1938).

To run a likelihood ratio test in R we will use the package *lmtest* (Zeileis and Hothorn 2002). We can start by comparing the ER, SYM, and ARD models as follows.

*: Nested models consist of a pair of models in which model A has model B as a special case. That is, we can write down model B as a particular example of model A. The ER model is simply an ARD model in which all rates are equal, and therefore is a special case of ARD. These two models can be compared. The **ordered** model does not have ER as a special case and so these are not nested model and cannot be compared using a likelihood ratio. 

```{r chunk11}
library(lmtest)
## likelihood-ratio test comparing ER & SYM
lrtest(fitER, fitSYM)
## likelihood-ratio test comparing ER & ARD
lrtest(fitER, fitARD)
## likelihood-ratio test comparing SYM & ARD
lrtest(fitSYM, fitARD)
```

1. Comparing ER with SYM we find P > 0.05, failing to reject the simpler ER model
2. Comparing ER with ARD we find P < 0.05, rejecting the simpler ER model in favor of the ARD model
3. Comparing SYM with ARD we find P < 0.05, rejecting the simpler SYM model in favor of the ARD model

We can also compare our reversible ordered and our directional models, and we can compare either of these models to the ARD model because both are special cases of ARD. 

We cannot compare the ordered and directional models to the ER model, even though the latter has greater complexity, because the ER model does not have either of our ordered models as a special case.

```{r chunk12}
## compare directional and ordered
lrtest(fitDirectional, fitOrdered)
## compare direction and ARD
lrtest(fitDirectional, fitARD)
## compare ordered and ARD
lrtest(fitOrdered, fitARD)
```

In all three comparisons, we failed to reject the simpler model (the directional model in comparisons 1 & 2, and the reversible ordered model in comparison #3).

We’re starting to build a picture that the ordered models may be better supported by the data than the ER, SYM, and ARD models.

This makes some biological sense. Based on developmental biology that a model in which evolution tends to involve successive losses (or gains) of digits may be more consistent with our data than a model in which any and all types of changes can occur — particularly if that model also supposes that these changes occur at the same rate (Brandley et al. 2008).

In addition, these two models generally involve the estimation of fewer parameters (five or ten) than the considerably more complex SYM (fifteen) and ARD (thirty) models.

We can't compare all models directly with the likelihood ration test, but we can using AIC.

```{r chunk13}
## accumulate AIC scores of all five models into
## a vector (sorted by complexity)
aic <- setNames(
    c(
        AIC(fitER), AIC(fitDirectional),
        AIC(fitOrdered), AIC(fitSYM), AIC(fitARD)
    ),
    c("ER", "Directional", "Ordered", "SYM", "ARD")
)
aic
```

The “directional” model is the model best supported by the data because it has the lowest AIC score.

AIC already takes the number of parameters estimated from the data into account, so there is no additional correction necessary. 

QUESTION: But what about AICc?

Many investigators consider AIC scores within around two units of each other to indicate similar or ambiguous support for the models under comparison (Burnham and Anderson 2003). Since our AIC score for the directional model is about three units better than the next best-supported model, we can feel reasonably confident that the directional model is the best of this set.

From AIC scores, we can also compute Akaike weights using `aic.w` from *phytools*. Akaike weights show the weight of evidence in support of each model in our data.

```{r chunk14}
aic.w(aic)
```

All the info about our model supports can be combined:

```{r chunk15}
round(data.frame(
    k = c(
        fitER$opt$k, fitDirectional$opt$k,
        fitOrdered$opt$k, fitSYM$opt$k, fitARD$opt$k
    ),
    logL = c(
        logLik(fitER), logLik(fitDirectional),
        logLik(fitOrdered), logLik(fitSYM), logLik(fitARD)
    ),
    AIC = aic, Akaike.w = as.vector(aic.w(aic))
), 3)
```

A directional model (in which digits tend to be lost but not reacquired) is best supported in the set, but an alternative model in which digits are both lost and regained, albeit in an ordered fashion, also garners some support.

The other models (ER, SYM, and ARD) are relatively poorly supported by the data.

The Mk model and its relatives provide a flexible and powerful way to evaluate the evolution of a discrete character on a phylogenetic tree.

## Practice Problems

### 6.1

Using the same squamate data set and tree as in the chapter, fit both an ordered and a directional model, but in which you constrain the ordered model to have just one rate of digit loss and a separate rate of digit gain (k = 2) and in which you constrain the directional model to a single rate of digit loss (k = 1). How can you compare these two models to the multi-rate ordered and directional models that we fit to the same data? What does this comparison reveal?

```{r problem1}
```

### 6.2

Now fit a model where digits can only be gained and never lost. Compare this model to the other candidates. What do you conclude?

```{r problem2}
```

### 6.3

Using a simulation study, determine the type I error rate of fitting an ARD model to a two-state character evolved under an ER model on a phylogenetic tree with 100 species.

```{r problem3}
```