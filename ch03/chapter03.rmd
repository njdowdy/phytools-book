## Chapter 3: Phylogenetic Generalized Least Squares

### Prepare for analyses

```{r chunk0}
library(phytools)
library(nlme)
set.seed(1234)
```

### Equivalence of contrasts regression and PGLS

Compare PIC regression with PGLS to on the same data to demonstrate that a numerically identical result is obtained.

To illustrate this, we’ll use an analysis of primate orbit morphology. We can relate eye size to the size of the skull and, eventually, to species diel activity patterns.

### Review: Fitting a regression model using PICs

```{r chunk1}

## load packages
## read data from file
primate.data <- read.csv("primateEyes.csv",
    row.names = 1,
    stringsAsFactors = TRUE
)
## inspect data
head(primate.data, 4)
```

```{r chunk2}
## read tree from file and inspect
primate.tree <- read.tree("primateEyes.phy")
print(primate.tree, printlen = 2)
```


```{r chunk3}
## extract orbit area from our data frame and add names
orbit.area <- setNames(
    primate.data[, "Orbit_area"],
    rownames(primate.data)
)
## extract skull length from our data frame and add names
skull.length <- setNames(
    primate.data[, "Skull_length"],
    rownames(primate.data)
)
## compute PICs on the log-transformed values of both traits
pic.orbit.area <- pic(log(orbit.area), primate.tree)
pic.skull.length <- pic(
    log(skull.length),
    primate.tree
)
## fit a linear regression to orbit area as a function of
## skull length, without an intercept term
pic.primate <- lm(pic.orbit.area ~ pic.skull.length + 0)
summary(pic.primate)
```

The slope of the fitted model is highly significant and has an estimated value of about 1.38.

Since both our traits are log-transformed, this slope gives the best-fitting allometric relationship between skull length and orbit area across all primates in our data set.

```{r chunk4}
## set plotting parameters
par(
    mfrow = c(1, 2),
    mar = c(5.1, 4.6, 2.1, 1.1)
)
## plot our raw data in the original space
plot(orbit.area ~ skull.length,
    log = "xy",
    pch = 21, bg = palette()[4], cex = 1.2,
    bty = "n", xlab = "skull length (cm)",
    ylab = expression(paste("orbit area (", mm^2, ")")),
    cex.lab = 0.8, cex.axis = 0.7, las = 1
)
mtext("(a)", line = 0, adj = 0, cex = 0.8)
## plot our phylogenetic contrasts
plot(pic.orbit.area ~ pic.skull.length,
    pch = 21,
    bg = palette()[4], cex = 1.2,
    bty = "n", xlab = "PICs for log(skull length)",
    ylab = "PICs for log(orbit area)",
    cex.lab = 0.8, cex.axis = 0.7, las = 1
)
mtext("(b)", line = 0, adj = 0, cex = 0.8)
## limit the plotting area to the range of our two traits
clip(
    min(pic.skull.length), max(pic.skull.length),
    min(pic.orbit.area), max(pic.orbit.area)
)
## add our fitted contrasts regression line
abline(pic.primate, lwd = 2)
```

Contrasts analysis of the primate orbit and skull length data set. (a) The original data, but with x and y axes transformed to a log-scale. (b) Independent contrasts. The line of panel (b) shows the fitted contrasts regression line.

### Fitting a linear regression model using PGLS

This uses a package called *nlme*.

PGLS can also be undertaken using the flexible R package caper by Orme et al. (2018).

We need to take our phylogenetic tree of primates and convert it into a special type of R object called a *correlation structure*. There are different types of correlation structures, but we will use the simplest for phylogenetic data: *corBrownian* (aka, "the Brownian Motion Model")

*corBrownian*  assumes that the correlation between the residual errors of any pair of species in the tree is directly proportional to the height above the root of the common ancestor of that pair.

When we create our correlation structure (corBM), we must specify the order of the taxa in our data, which is done using the argument form. Otherwise, it’ll be assumed that the order of the rows in our input data frame matches the order of the tip labels of the tree, which can be very dangerous!

```{r chunk6}
spp <- rownames(primate.data)
corBM <- corBrownian(phy = primate.tree, form = ~spp)
corBM
```

You can use non-ultrametric trees with PLGS, but it is more complicated to set up.

However, working with ultrametric phylogenies is generally recommended except under circumstances in which some of the lineages in the tree are meant to represent extinct taxa.

In addition to the "corStruct" object, for a non-ultrametric tree, users should also create an object of class "varFixed" with variances proportional to the total tree height to the end of each tip.

Our "varFixed" object is then passed to gls via the argument weights.

#### TANGENT: Ultrametric trees

PhylANOVA, PIC, and other analyses will assume your tree will be both ultrametric and dichotomous. 

What is "ultrametric"? Trees where all of the path lengths from the root to the tips are equal.

What is "dichotomous"? A fully bifurcated, resolved tree.

RaxML and IQTREE2 do not create ultrametric trees. You can make those output trees ultrametric using the *chronos* function in *ape*.

```{r chunk7}
is.ultrametric(primate.tree) # check if the tree is ultrametric
# lambda is a rate-smoothing parameter
# generally, we want lambda to be small, so there is very little smoothing
ultrametric_tree <- chronos(primate.tree, lambda = 0)
is.ultrametric(ultrametric_tree) # check if the tree is ultrametric
```

If there is a problem with the above, you can force it with *force.ultrametric* from *phytools*. But note:

> "Note that neither of these should be treated as formal statistical methods for inferring an ultrametric tree. Rather, this method can be deployed when a genuinely ultrametric tree read from file fails is.ultrametric for reasons of numerical precision."

```{r chunk8}
force.ultrametric(ultrametric_tree, method = "nnls")
```

You can make those output trees dichotomous using the *multi2di* function in *ape*.

```{r chunk9}
clean_tree <- multi2di(ultrametric_tree, random = TRUE)
```

Now we’re ready to fit our linear model.

```{r chunk10}
pgls.primate <- gls(log(Orbit_area) ~ log(Skull_length),
    data = primate.data, correlation = corBM
)
summary(pgls.primate)
```

The model summary tells us the significance (P-values) of our different model coefficients, as well as other relevant information about model fit (Ives 2019).

Notably, our model summary does not include a value for **R**^2^. In fact, **R**^2^ (as a fraction of variance explained by the model) does not extend well to model fitting with GLS. 

Some pseudo-**R**^2^ measures have been proposed (Ives 2019), but we feel that a comprehensive discussion of these is beyond the scope of this chapter.

### Comparing PICs and PGLS

```{r chunk11}
coef(pic.primate)
coef(pgls.primate)
abs(coef(pic.primate)[1] - coef(pgls.primate)[2])
```

The fitted PGLS model was able to include an intercept term, while the contrast regression did not.

This is simply because in PGLS, the data are not transformed into a new space before analysis, and as such, the coefficient doesn't need to be dropped from the fitted model.

PIC regression is a special case of linear regression using PGLS (Blomberg et al. 2012) where the correlation structure of the residual error (as set by corBrownian) is one where the expected correlation between species is directly proportional to their fraction of common ancestry since the root.

### Assumptions of PGLS

```{r chunk12}
coef(pic.primate)
coef(pgls.primate)
abs(coef(pic.primate)[1] - coef(pgls.primate)[2])
```

The fitted PGLS model was able to include an intercept term, while the contrast regression did not.

This is simply because in PGLS, the data are not transformed into a new space before analysis, and as such, the coefficient doesn't need to be dropped from the fitted model.

PIC regression is a special case of linear regression using PGLS (Blomberg et al. 2012) where the correlation structure of the residual error (as set by corBrownian) is one where the expected correlation between species is directly proportional to their fraction of common ancestry since the root.

```{r chunk13}
## set the random number generator seed
set.seed(88)
## simulate a random 5-taxon tree
tree <- pbtree(n = 5, scale = 10, tip.label = LETTERS[5:1])
## subdivide our plotting area into two panels
par(mfrow = c(2, 1))
## plot the tree
plotTree(tree,
    mar = c(3.1, 1.1, 4.1, 1.1), fsize = 1.25,
    ylim = c(0.5, 5.4)
)
## add a horizontal axis
axis(1)
## add edge labels giving the branch lengths
edgelabels(round(tree$edge.length, 2),
    pos = 3,
    frame = "none", cex = 0.9
)
mtext("(a)", line = 1, adj = 0)
## switch to the second panel
plot.new()
## set new plot margins and plot dimensions
par(mar = c(3.1, 1.1, 4.1, 1.1))
plot.window(xlim = c(0, 6), ylim = c(0, 6))
## add a grid of lines for our correlation matrix
lines(c(0, 6, 6, 0, 0), c(0, 0, 6, 6, 0))
for (i in 1:5) lines(c(i, i), c(0, 6))
for (i in 1:5) lines(c(0, 6), c(i, i))
## compute the assumed correlation structure
V <- cov2cor(vcv(tree)[LETTERS[1:5], LETTERS[1:5]])
## print it into the boxes of our grid
for (i in 1:5) text(i + 0.5, 5.5, LETTERS[i], cex = 1.1)
for (i in 1:5) text(0.5, 5.5 - i, LETTERS[i], cex = 1.1)
for (i in 1:5) {
    for (j in 1:5) {
        text(0.5 + i, 5.5 - j,
            round(V[i, j], 2),
            cex = 1.1
        )
    }
}
mtext("(b)", line = 1, adj = 0)
```

The correlation between taxa C and E is the distance from the root of the common ancestor of C and E (4.36) divided by the total length of the tree (10) and obtain the correlation (0.44).

The correlation between taxa C and D is the distance from the root of the common ancestor of C and E (4.36 + 2.96 = 7.32) divided by the total length of the tree (10) and obtain the correlation (0.73).

Taxa which are more closely related will have higher correlation values.

###  Alternative models for the residual error

We can implement models that are more complex than the Brownian Motion Model. 

#### Pagel's Lambda

A common method introduces the λ parameter as a multiplier of the off-diagonal elements of the matrix (Pagel 1999a).

This model has both OLS (λ = 0) and standard PGLS (λ = 1) as special cases. We can figure out which value of λ is best supported by the pattern in our data by estimating it using a procedure called maximum likelihood*.

\*: by default, *gls* uses REML, "REstricted Maximum Likelihood", for estimation, rather than maximum likelihood. REML uses the likelihood of a transformation of the data, rather than the data themselves. This only becomes important if we want to compare among different models for the correlation structure, in which case we can switch to maximum likelihood by setting method="ML"

The "value" parameter of *corPagel* corresponds to the λ value. Note that the particular value we specify for λ is just a starting value. The final value of λ will be estimated jointly with our fitted regression model.

The specific starting condition for λ isn’t too important, but we should be sure to choose a number that is on the range over which λ is defined. Since λ is always defined between 0 and 1, the initial value of λ = 0.5 is fine.

``` {r chunk14}
corLambda <- corPagel(value = 0.5, phy = primate.tree, form = ~spp)
corLambda
```

```{r chunk15}
pgls.Lambda <- gls(log(Orbit_area) ~ log(Skull_length),
    data = primate.data, correlation = corLambda
)
summary(pgls.Lambda)
```

This result shows that the ML estimate of lambda, 1.01, is extremely close to 1 but a tiny bit higher.

That means that under our model, close relatives have correlated residuals and even a bit more so than they would under our original model — and we still conclude that there is an evolutionary correlation between the two traits.

#### Other models

The principles that underlie working with the alternative error structures in R are basically the same.

We first create an uninitialized object of class "corStruct", and then we optimize the parameters of the error structure jointly with our model.

Typically, each error structure has corBrownian as a special case, and many also have OLS (for the λ model, λ = 0) as a special case as well.

To pick the model that best fits our data, it is a valid exercise to fit alternative models for the error structure to our tree and data set and compare them (fitting the models in question with method="ML" instead of method="REML").

### Phylogenetic ANOVA and ANCOVA

PGLS more easily fits a linear model that includes one or more factors as independent variables (i.e., an ANOVA model) or a combination of continuous and discrete factors (i.e., an ANCOVA) compared to PIC regression.

We can test the hypothesis that diel activity pattern — nocturnal, diurnal, or cathemeral — affects relative eye size. We say relative eye size here because our analysis is going to also control for allometry by including skull size as an additional covariate.

This kind of model is called an ANCOVA model, and we’ll be assuming that the correlation structure of the residual error is given by the phylogeny. We’ll thus call our model a phylogenetic generalized ANCOVA.

``` {r chunk16}
primate.ancova <- gls(
    log(Orbit_area) ~ log(Skull_length) +
        Activity_pattern,
    data = primate.data,
    correlation = corBM
)
anova(primate.ancova)
```

There is a significant effect of activity pattern on orbit area after controlling for the also significant allometric effect of skull size.

``` {r chunk17}
## set the margins of our plot using par
par(mar = c(5.1, 5.1, 2.1, 2.1))
## set the point colors for the different levels
## of our factor
pt.cols <- setNames(
    c("#87CEEB", "#FAC358", "black"),
    levels(primate.data$Activity_pattern)
)
## plot the data
plot(Orbit_area ~ Skull_length,
    data = primate.data, pch = 21,
    bg = pt.cols[primate.data$Activity_pattern],
    log = "xy", bty = "n", xlab = "skull length (cm)",
    ylab = expression(paste("orbit area (", mm^2, ")")),
    cex = 1.2, cex.axis = 0.7, cex.lab = 0.8
)
## add a legend
legend("bottomright", names(pt.cols),
    pch = 21, pt.cex = 1.2,
    pt.bg = pt.cols, cex = 0.8
)
## create a common set of x values to plot our
## different lines for each level of the factor
xx <- seq(min(primate.data$Skull_length),
    max(primate.data$Skull_length),
    length.out = 100
)
## add lines for each level of the factor
lines(xx, exp(predict(primate.ancova,
    newdata = data.frame(
        Skull_length = xx,
        Activity_pattern = as.factor(rep("Cathemeral", 100))
    )
)),
lwd = 2, col = pt.cols["Cathemeral"]
)
lines(xx, exp(predict(primate.ancova,
    newdata = data.frame(
        Skull_length = xx,
        Activity_pattern = as.factor(rep("Diurnal", 100))
    )
)),
lwd = 2, col = pt.cols["Diurnal"]
)
lines(xx, exp(predict(primate.ancova,
    newdata = data.frame(
        Skull_length = xx,
        Activity_pattern = as.factor(rep("Nocturnal", 100))
    )
)),
lwd = 2, col = pt.cols["Nocturnal"]
)
```

Results of our ANCOVA analysis on primate orbit area as a function of skull length and diel activity pattern.

Our results show that we can best predict primate orbital area if we account for both their skull length and activity pattern.

Nocturnal species have large orbital areas relative to their skull size (the black line above), compared to cathemeral or diurnal species.

Since we didn’t include an interaction term in our final model, we’re assuming that the allometric slope of the orbital–skull relationship is equal across all activity patterns.

#### Modeling Interaction Terms

We could’ve also fit an ANCOVA model with an interaction between our discrete factor, diel activity pattern, and the covariate.

For our example, instead of 

log(Orbit_area) ∼ log(Skull_length) + Activity_pattern

would be written as:

log(Orbit_area) ∼ log(Skull_length) * Activity_pattern

For the primate data, the interaction term is non-significant; however, if it had been significant, this could be interpreted as evidence for a difference in the slope of the relationship between the two continuous variables as a function of the factor: a common use for ANCOVA models.

### Summary

PGLS is a more flexible method to test for evolutionary correlations, and it allows for alternative models of the residual error in y given our fitted model, multiple predictor variables, and both continuous and discrete predictors.

All assumptions in PGLS are assumptions about the distribution of species’ residuals from the linear model.

### Practice Problems

#### 3.1

In practice problem 2.2 of the previous chapter, you used Asian barbet data to test whether the variable *Lnalt* varied as a function of wing. Again, using these data from Asian barbets (BarbetTree.nex and Barbetdata_mod.csv), carry out the same analysis using PGLS. Confirm that you get the same results from PGLS and PICs. What happens to your fitted model if you also estimate λ using the *corPagel* function?

#### 3.2

If you multiply all the branches of your phylogenetic tree by 100, will your PGLS analysis change? Why or why not? Can you confirm this using R code?

#### 3.3

Use the data files from chapter 1 to run a phylogenetic ANCOVA for anoles testing for the effect of body size ("SVL") and ecomorphological state ("ecomorph") on forelimb length ("FLL") in anoles, using an Ornstein–Uhlenbeck model, as implemented in the *ape* function corMartins, as your correlational structure for the residual error of the model. You can use the data files from chapter 1, but you will need to do some work to combine data across files.